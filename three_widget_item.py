import os

from PyQt5.QtGui import QImage, QIcon, QPixmap
from PyQt5.QtWidgets import *


class TreeWidgetItem(QWidget):
    def __init__(self, index, parent=None):
        super(TreeWidgetItem, self).__init__()
        self.index = index
        self.parent = parent
        hbox = QHBoxLayout()
        self.setLayout(hbox)

        self.title = QLabel("")
        self.title.setMinimumWidth(30)

        self.edit_text = QLineEdit()

        self.button = QPushButton()
        self.button.setIcon(QIcon(QPixmap(os.path.dirname(os.path.abspath(__file__)) + "/assets/icons/filter.png")))

        self.delete_button = QPushButton("Filtered")
        self.delete_button.setIcon(QIcon(QPixmap(os.path.dirname(os.path.abspath(__file__)) + "/assets/icons/delete.png")))

        self.delete_button_all = QPushButton("All")
        self.delete_button_all.setIcon(QIcon(QPixmap(os.path.dirname(os.path.abspath(__file__)) + "/assets/icons/delete.png")))

        hbox.addWidget(self.title)
        hbox.addWidget(self.edit_text)
        hbox.addWidget(self.button)
        hbox.addWidget(self.delete_button)
        hbox.addWidget(self.delete_button_all)

        self.button.clicked.connect(self.filter)
        self.delete_button.clicked.connect(self.delete)
        self.delete_button_all.clicked.connect(self.delete_all)

    def set_title(self, title):
        self.title.setText(title)

    def filter(self):
        if self.parent is not None:
            self.parent.set_filter(self.index, self.edit_text.text())

    def delete(self):
        if self.parent is not None:
            self.parent.delete(self.index, self.edit_text.text())
        pass

    def delete_all(self):
        if self.parent is not None:
            self.parent.delete_all(self.index, self.edit_text.text())
        pass
