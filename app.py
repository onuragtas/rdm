# -*- coding:utf8 -*-
import sys
from threading import Thread

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDesktopWidget

from gui import Main
from add_server import AddServer
from models.server import Server
from tab_widget_item import TabWidgetItem
from three_widget_item import TreeWidgetItem
from utils.database import Database
from utils.redis import RedisClient


class MainDialog(QtWidgets.QMainWindow, Main.Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent)
        self.setupUi(self)
        self.showMaximized()
        Database.get_db().connect()
        Database.get_db().create_tables([Server])

        self.db = None
        self.parent_name = None
        self.filter_text = "*"
        self.load_limit = 100
        self.showed_count = 0

        self.tabWidget.setMinimumWidth(QDesktopWidget().screenGeometry().width()/2)
        self.tabWidget.setTabsClosable(True)
        self.tabWidget.tabCloseRequested.connect(self.close_tab)

        self.list_server_btn.clicked.connect(self.new_page)
        self.treeview.itemClicked.connect(self.handle)

        self.prepare_data()

    def new_page(self):
        add_server = AddServer(self)
        add_server.show()

    def prepare_data(self):
        self.treeview.clear()
        for server in Server.select():
            database_count = RedisClient(server.host, server.port).get_databases()
            parent = QtWidgets.QTreeWidgetItem(self.treeview)
            parent.setText(0, server.host)
            for i in range(0, database_count):
                child = QtWidgets.QTreeWidgetItem(parent)
                child.setText(0, "DB" + str(i))
                three_widget_item = TreeWidgetItem(child, self)
                self.treeview.setItemWidget(child, 0, three_widget_item)

    def delete(self, item, filter_text):
        index, parent_name, db, item_name = self.get_index_parent_name(item)

        server = Server.get(Server.host == parent_name)
        client = RedisClient(server.host, server.port, db)
        try:
            client.delete(filter_text)
            self.handle(item)

        except Exception as e:
            print(e)

    def delete_all(self, item, filter_text):
        index, parent_name, db, item_name = self.get_index_parent_name(item)
        try:
            for i in reversed(range(item.childCount())):
                item.removeChild(item.child(i))
        except Exception as e:
            print(e)

        server = Server.get(Server.host == parent_name)
        client = RedisClient(server.host, server.port, db)
        try:
            client.delete_all_keys()
            self.handle(item)
        except Exception as e:
            print(e)

    def set_filter(self, item, filter_text):
        self.filter_text = filter_text
        self.showed_count = 0
        index, parent_name, db, item_name = self.get_index_parent_name(item)
        try:
            for i in reversed(range(item.childCount())):
                item.removeChild(item.child(i))
        except Exception as e:
            print(e)

        server = Server.get(Server.host == parent_name)
        client = RedisClient(server.host, server.port, db)
        try:
            Thread(target=self.add_item, args=(client, item, self.filter_text)).start()
            print("finish keys")
        except Exception as e:
            print(e)

        item.setExpanded(True)

    def handle(self, item):
        index, parent_name, db, item_name = self.get_index_parent_name(item)
        clear = False
        if self.db != db or self.parent_name != parent_name:
            self.showed_count = 0
            self.db = db
            self.parent_name = parent_name
            clear = True

        if index == 0:
            pass
        elif index == 1:
            try:
                if item.text(0) == 'Load More':
                    self.showed_count += self.load_limit
                    parent = item.parent()

                    root = self.treeview.invisibleRootItem()
                    for item in self.treeview.selectedItems():
                        (item.parent() or root).removeChild(item)

                    item = parent

                    if clear:
                        for i in reversed(range(item.childCount())):
                            item.removeChild(item.child(i))
                else:
                    for i in reversed(range(item.childCount())):
                        item.removeChild(item.child(i))
            except Exception as e:
                print(e)

            server = Server.get(Server.host == parent_name)
            client = RedisClient(server.host, server.port, db)
            try:
                Thread(target=self.add_item, args=(client, item, self.filter_text)).start()
                print("finish keys")
            except Exception as e:
                print(e)
        else:
            server = Server.get(Server.host == parent_name)
            client = RedisClient(server.host, server.port, db)
            try:

                if len(self.get_indices(item_name)) == 0:
                    tab = TabWidgetItem(client, self.tabWidget)
                    tab.set_key(item_name)
                    self.tabWidget.addTab(tab, item_name)
                self.tabWidget.setCurrentIndex(self.get_indices(item_name)[0])
            except Exception as e:
                print(e)

        if item is not None:
            item.setExpanded(True)

    def get_indices(self, tab_name):
        return [index for index in range(self.tabWidget.count())
                if tab_name == self.tabWidget.tabText(index)]

    def add_item(self, client, item, key="*"):
        if key == "":
            key = "*"

        keys, load_more = client.get_all_keys(key, self.showed_count, (self.showed_count+self.load_limit + 1))
        try:
            for key in keys:
                Thread(target=self.add_item_tree, args=(item, key)).start()

            if load_more:
                Thread(target=self.add_item_tree, args=(item, "Load More".encode())).start()
            print("finish")
        except Exception as e:
            print(e)
            pass

    @staticmethod
    def add_item_tree(item: QtWidgets.QTreeWidgetItem, key):
        tree_item = QtWidgets.QTreeWidgetItem(item)
        tree_item.setText(0, key.decode())

    def get_index_parent_name(self, item):
        check_item_name = item.text(0)
        parent_name = None
        db = None
        parent = item
        index = -1
        item_name = item.text(0)
        for i in range(0, 100):
            try:
                parent_name = parent.text(0)

                if parent_name.find("DB") != -1:
                    db = int(parent_name.replace("DB", ''))

                parent = parent.parent()
                index += 1
            except Exception as e:
                print(e)
                break

        if check_item_name == 'Load More':
            index = 1

        return [index, parent_name, db, item_name]

    def close_servers(self):
        self.prepare_data()

    def remove_current_item_treeview(self):
        root = self.treeview.invisibleRootItem()
        for item in self.treeview.selectedItems():
            (item.parent() or root).removeChild(item)

    def close_tab(self, index):
        widget = self.tabWidget.widget(index)
        widget.deleteLater()
        self.tabWidget.removeTab(index)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    form = MainDialog()
    form.show()
    app.exec_()
