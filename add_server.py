# -*- coding:utf8 -*-
import gui.AddServer as Ui_gui
from PyQt5 import QtGui, QtWidgets
from threading import Thread

from models.server import Server
from utils.redis import RedisClient


class AddServer(QtWidgets.QMainWindow, Ui_gui.Ui_MainWindow):
    def __init__(self, parent=None):
        super(AddServer, self).__init__(parent)
        self.selected_host = None
        self.setupUi(self)

        self.parent = parent

        self.model = QtGui.QStandardItemModel()
        self.server_list_view.setModel(self.model)

        self.prepare_data()

        self.test_server_btn.clicked.connect(self.test_connection)
        self.save_server_btn.clicked.connect(self.save_connection)
        self.server_list_view.clicked.connect(self.select_server)
        self.add_server_btn.clicked.connect(self.add_new_server)
        self.delete_server_btn.clicked.connect(self.delete_server_func)

    def prepare_data(self):
        self.model.clear()
        for server in Server.select():
            item = QtGui.QStandardItem(server.host)
            self.model.appendRow(item)

    def add_model_new(self):
        item = QtGui.QStandardItem("New")
        self.model.appendRow(item)

    def test_connection(self):
        Thread(target=self.check_connection, args=()).start()

    def check_connection(self):
        client = RedisClient(self.server_host_edit.text(), self.server_port_edit.text())
        if client.is_connect():
            self.label_5.setText("Connected")
        else:
            self.label_5.setText("Not Connected")

    def save_connection(self):
        if self.selected_host is None:
            if Server.create(
                    host=self.server_host_edit.text(),
                    port=self.server_port_edit.text(),
                    username=self.server_username_edit.text(),
                    password=self.server_password_edit.text()
            ):
                self.label_5.setText("Created")
            else:
                self.label_5.setText("Not Created")
        else:
            Server.update(
                host=self.server_host_edit.text(),
                port=self.server_port_edit.text(),
                username=self.server_username_edit.text(),
                password=self.server_password_edit.text()
            ).where(Server.host == self.selected_host)
            self.selected_host = None
        self.prepare_data()

    def select_server(self, model_index):
        try:
            self.selected_host = self.server_list_view.model().itemData(model_index)[0]
            row = Server.get(Server.host == self.selected_host)
            self.server_host_edit.setText(row.host)
            self.server_port_edit.setText(row.port)
            self.server_username_edit.setText(row.username)
            self.server_password_edit.setText(row.password)
            self.label_5.setText("")
        except Exception as e:
            self.selected_host = None
            print(e)

    def add_new_server(self):
        self.selected_host = None
        self.add_model_new()

    def delete_server_func(self):
        if self.selected_host is None:
            self.label_5.setText("Select Server")
        else:
            Server.get(host=self.selected_host).delete_instance()
            self.prepare_data()

    def closeEvent(self, event):
        self.parent.close_servers()
        event.accept()
