import os

from peewee import *

db = SqliteDatabase(os.path.dirname(os.path.abspath(__file__)) + "/../rdm.db")


class Database:
    def __init__(self):
        pass

    @staticmethod
    def get_db():
        return db

