from redis import Redis


class RedisClient():
    def __init__(self, host, port, db=0):
        self.client = Redis(host=host, port=port, db=db, socket_timeout=5, socket_connect_timeout=5)

    def is_connect(self):
        try:
            self.client.ping()
            return True
        except:
            return False

    def get_databases(self):
        return int(self.client.config_get('databases')['databases'])

    def get_all_keys(self, key="*", start=0, finish=None):
        keys = self.client.keys(key)
        i = 0
        result = []
        for _key in keys:
            if i >= start:
                result.append(_key)

            if i > finish:
                break

            i += 1

        load_more = False
        if len(result) < len(keys):
            load_more = True
        return result, load_more

    def get(self, key):
        return self.client.get(key)

    def delete(self, key):
        for i in self.client.scan_iter(key):
            self.client.delete(i)

    def delete_all_keys(self):
        return self.client.flushdb()

    def get_ttl(self, key):
        return self.client.ttl(key)
