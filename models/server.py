from models.base_model import BaseModel
from peewee import *


class Server(BaseModel):
    host = TextField()
    port = TextField()
    username = TextField()
    password = TextField()
