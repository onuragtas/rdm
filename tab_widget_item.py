import json

import phpserialize as phpserialize
from PyQt5.QtWidgets import QTabWidget, QWidget, QFormLayout, QLineEdit, QHBoxLayout, QLabel, QPushButton, QComboBox, \
    QTextEdit

from borderlayout import BorderLayout
from utils.redis import RedisClient


class TabWidgetItem(QWidget):
    def __init__(self, client: RedisClient, tab_widget: QTabWidget):
        super(TabWidgetItem, self).__init__()
        self.data = None
        self.tab_widget = tab_widget
        self.client = client
        self.key = None
        self.changed = None
        self.process = None

        self.border_layout = BorderLayout()

        top = QWidget()
        center = QWidget()

        self.lineedit = QLineEdit()
        self.rename_btn = QPushButton("Rename")
        self.delete_btn = QPushButton("Delete")
        self.reload_btn = QPushButton("Reload")
        self.ttl_time = QLabel()
        self.combo_box = QComboBox()
        self.combo_box.addItem("Type")
        self.combo_box.addItem("Text")
        self.combo_box.addItem("Json")
        self.combo_box.addItem("Unserialize")

        hboxLayout = QHBoxLayout()
        hboxLayout.addWidget(QLabel("Key:"))
        hboxLayout.addWidget(self.lineedit)
        hboxLayout.addWidget(self.rename_btn)
        hboxLayout.addWidget(self.ttl_time)
        hboxLayout.addWidget(self.delete_btn)
        hboxLayout.addWidget(self.reload_btn)
        hboxLayout.addWidget(self.combo_box)
        top.setLayout(hboxLayout)

        hboxLayout_center = QHBoxLayout()
        self.key_value = QTextEdit()
        hboxLayout_center.addWidget(self.key_value)
        center.setLayout(hboxLayout_center)

        self.border_layout.addWidget(top, BorderLayout.Top)
        self.border_layout.addWidget(center, BorderLayout.Center)
        self.setLayout(self.border_layout)

        self.reload_btn.clicked.connect(self.reload)
        self.delete_btn.clicked.connect(self.delete)
        self.combo_box.currentTextChanged.connect(self.on_combobox_changed)

    def tab_ui(self, tab: QWidget, key):
        layout = QFormLayout()
        layout.addRow("Key", QLineEdit())
        tab.setTabText(0, key)
        tab.setLayout(layout)

    def set_key(self, key):
        self.lineedit.setText(key)
        self.key = key
        self.get_detail()

    def get_detail(self):
        if self.data is None:
            self.data = self.client.get(self.key)
        ttl = self.client.get_ttl(self.key)
        self.ttl_time.setText(str(ttl))
        selected = self.combo_box.currentText()
        if selected == "Type":
            self.process = False
            try:
                data = json.dumps(self.convert(phpserialize.unserialize(self.data)), indent=4, sort_keys=True)
                self.key_value.setText(data)
                self.combo_box.setCurrentText("Unserialize")
                self.process = True
            except Exception as e:
                print(e)

            try:
                data = json.dumps(json.loads(self.data.decode()), indent=4, sort_keys=True)
                self.key_value.setText(data)
                self.combo_box.setCurrentText("Json")
                self.process = True
            except Exception as e:
                print(e)

            if self.process is False:
                self.key_value.setText(self.data.decode())
                self.combo_box.setCurrentText("Text")

            self.process = None

        else:
            if selected == "Text":
                self.key_value.setText(self.data.decode())
            elif selected == "Json":
                try:
                    data = json.dumps(json.loads(self.data.decode()), indent=4, sort_keys=True)
                    self.key_value.setText(data)
                except Exception as e:
                    print(e)
            elif selected == "Unserialize":
                try:
                    data = json.dumps(self.convert(phpserialize.unserialize(self.data)), indent=4, sort_keys=True)
                    self.key_value.setText(data)
                except Exception as e:
                    print(e)

    def on_combobox_changed(self):
        if self.process is None:
            self.get_detail()

    def reload(self):
        self.get_detail()

    def convert(self, data):
        if isinstance(data, bytes):
            return data.decode()
        if isinstance(data, dict):
            return dict(map(self.convert, data.items()))
        if isinstance(data, tuple):
            return tuple(map(self.convert, data))
        if isinstance(data, list):
            return list(map(self.convert, data))
        return data

    def delete(self):
        self.client.delete(self.key)
        self.tab_widget.removeTab(self.tab_widget.currentIndex())
